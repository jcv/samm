# 💉 Serum Automated Market Maker

## 🏃‍ Quickstart

Let’s assume you have a server set up already, with [docker](https://www.docker.com/) installed. This Quickstart will guide you through configuring and running SAMM to add orders to Serum’s ETH/USDT market using a newly-created wallet. (You’ll need to provide your own funds for the orders though!)

Throughout this guide you’ll see my private key, accounts and transaction IDs as well as the commands and parameters I use. (I’ll be removing the funds from this account as soon as I’m finished so don’t even bother trying to load that account!)

### 0. ☠️ Risks

OK, now is the time to question if you should really do this. No-one here is going to take any responsibility for your actions. It’s all on you. Are you certain you want to do this? Are you sure you want to run this code? Have you even checked it to make sure it’s not going to steal your money? Are you aware of the risks, not just from constant-product market making, but from markets in general, plus bugs, developer incompetence, malfeasance and lots of other bad stuff?

### 1. 🦺 Prerequisites

To run this quickstart you’ll need:
* A server set up with [docker](https://www.docker.com/) installed and configured.
* Some SOL for transaction costs.
* A lot of ether for placing market orders.

### 2. 📂 Directories And Files

Our server will keep all its files under `/var/sammd`, so run the following commands to set up the necessary directories and files:

```
$ mkdir -p /var/sammd/{bin,auditlogs}
$ chown 1000:1000 /var/sammd/wallet.json /var/sammd/auditlogs
$ touch /var/sammd/wallet.json
```
(Don’t type the $ prompt - that’s to show you the unix prompt where the command starts.)

### 3. 📜 Scripts

Next, create a couple of shell scripts to make running the container easier:
```
$ cat << EOF > /var/sammd/bin/sammd
#! /usr/bin/env bash
docker run --rm -d --name sammd \\
    -v /var/sammd/wallet.json:/home/node/app/wallet.json \\
    -v /var/sammd/auditlogs:/home/node/app/auditlogs \\
    opinionatedgeek/samm:v0.1 \\
    \$@
docker logs --follow sammd
EOF
```
```
$ cat << EOF > /var/sammd/bin/samm
#! /usr/bin/env bash
docker run --rm -it --name=samm \\
    -v /var/sammd/wallet.json:/home/node/app/wallet.json \\
    -v /var/sammd/auditlogs:/home/node/app/auditlogs \\
    opinionatedgeek/samm:v0.1 \\
    \$@
EOF
```
```
$ chmod +x /var/sammd/bin/sammd /var/sammd/bin/samm
```

### 4. 👛 Create The Wallet

Run the following command to create your wallet:
```
$ /var/sammd/bin/samm create-wallet --overwrite
2020-10-01T11:29:59.464Z 📄 Wallet file created at: wallet.json
2020-10-01T11:29:59.528Z 📄 Wallet account address: CsKuESr3J5uNXaGwUhXucP3uoSPGU8pTWwb1Sa5mf2MC
```

This will create a Solana wallet and write its mnemonic seed to /var/sammd/wallet.json. **Looking after this file is entirely your responsibility. If you lose this file, you lose the private key for all the funds in the wallet. If you give it to someone else you give them the entire contents of your wallet.**

Using Docker may lead to permission errors on some systems. If this happens you’ll see a message like:
```
$ /var/sammd/bin/samm create-wallet --overwrite
2020-10-16T17:37:46.794Z 🛑 Error:
  Error: File wallet.json exists but is not writable by user 'node' (ID 1001:1001).
      at Wallet.save (/home/node/app/lib/wallet.js:103:23)
      at Object.<anonymous> (/home/node/app/bin/create-wallet:12:9)
      at Module._compile (internal/modules/cjs/loader.js:1076:30)
      at Object.Module._extensions..js (internal/modules/cjs/loader.js:1097:10)
      at Module.load (internal/modules/cjs/loader.js:941:32)
      at Function.Module._load (internal/modules/cjs/loader.js:782:14)
      at Function.executeUserEntryPoint [as runMain] (internal/modules/run_main.js:72:12)
```
You can fix this by changing ownership of the `wallet.json` file like so - note the `1001:1001` parameter comes from the ID specified in the error message, and may be different on your system):
```
$ chown 1001:1001 /var/sammd/wallet.json /var/sammd/auditlogs
```
Once successfully created, if you look at the file you’ll see the mnemonic seed of your private key. **Keep this secret!**

It should look something like this, but with different seed words:
```
$ cat /var/sammd/wallet.json
{
    "mnemonic": "seat impose palace indoor apart december tumble soup rain satoshi chat abandon"
}
```

### 5. 💸 Add Some SOL

Transfer some SOL into the account just created. SOL tokens are needed for running operations on the Solana blockchain, similar to the way ether is used on Ethereum.

How you transfer the SOL is up to you, and dependent on where you actually have SOL.

I used [sollet](https://sollet.io) to transfer 2 SOL to **CsKuESr3J5uNXaGwUhXucP3uoSPGU8pTWwb1Sa5mf2MC**, the address shown when creating the wallet above. When the transfer completes (it’s very fast!) it appears in the wallet and you can check that using the `balance` command:
```
$ /var/sammd/bin/samm balance
2020-10-01T11:34:05.473Z 📄 Solana:                             2 SOL
```

### 6. 🧾 Create SPL Accounts For ETH And USDT

Unlike Ethereum, Solana uses a different account and address for each different type of token. Typically, SAMM will have a main account which contains the SOL used for operations, plus one additional address for each token type it uses. In this quickstart we’re using 2 additional token types - SPL Wrapped ETH and SPL Wrapped USDT - and accounts need to be created for these.

SAMM can take care of this for us with the following command:

```
$ /var/sammd/bin/samm init-market --market ETH/USDT
2020-10-01T11:37:18.816Z 📄 No account for base token ETH. Creating...
2020-10-01T11:37:25.300Z 📄 Account created for base token ETH at address 5S3YrMvnFSJoDXyEbXzrka9wrppKjBAZvAfSVTErdWiM
2020-10-01T11:37:25.908Z 📄 No account for quote token USDT. Creating...
2020-10-01T11:37:30.979Z 📄 Account created for quote token USDT at address EuFT3Dxhuxsav2j8BFRnkqt9HmyPvr6tvchMR53WgoYQ
```

As you can see, this created two SPL accounts, one for ETH, one for USDT, and it printed out their addresses.

(It’s safe to re-run this command too - it won’t create a new account if one already exists for that token type in its account.)

You can verify these accounts and addresses by importing your mnemonic seed phrase into [sollet](https://sollet.io).

You can see that used a little bit of SOL:
```
/var/sammd/bin/samm balance --market ETH/USDT
2020-10-01T11:44:06.514Z 📄 Solana:                    1.99590144 SOL
2020-10-01T11:44:09.069Z 📄 Wrapped Ethereum:                   0 ETH
2020-10-01T11:44:09.070Z 📄 Wrapped USDT:                      0 USDT
2020-10-01T11:44:09.545Z 📄 Notional total:                    0 USDT
```

### 7. 💰 Add Some ETH

Transfer some ETH into the ETH address you just created.

```
$ /var/sammd/bin/samm balance --market ETH/USDT
2020-10-01T11:46:32.939Z 📄 Solana:                    1.99590144 SOL
2020-10-01T11:46:34.991Z 📄 Wrapped Ethereum:             8.63585 ETH
2020-10-01T11:46:34.993Z 📄 Wrapped USDT:                      0 USDT
2020-10-01T11:46:35.644Z 📄 Notional total:         3169.7887425 USDT
```

You can see the `balance` command calculating how much the ETH is worth in USDT terms. The ‘notional total’ is the sum of USDT in the USDT account and the value of ETH in the ETH account in USDT terms based on the current ‘best bid’ on the Serum market. It can be handy (although sometimes misleading) to have one overall value to compare.

### 8. 💱 Convert Half The ETH To USDT

To keep things simple, we won’t transfer any USDT in. We’ll assume you have the entirety of the stake you want to use for market-making all in ETH, and we need to convert some to USDT for the constant product function to work.

Instead of manually calculating things, there’s a handy `rebalance` command. This tries to shift the balances of your ETH account and your USDT account so they have (roughly) 50/50 balance. It should work with any different balances, so if you have 40% in ETH and 60% in USDT it should still figure out the right values from the current spread on Serum and sell or buy what’s appropriate.

But let’s try a simulated run first:
```
$ /var/sammd/bin/samm rebalance --market ETH/USDT --simulate
2020-10-01T12:32:27.459Z 📄 Spread: « 367.31 / 367.32 USDT »
2020-10-01T12:32:27.463Z 📄 Mid price: 367.315 USDT
2020-10-01T12:32:27.465Z 📄 Ratio should be: 0.00272245892490097056
2020-10-01T12:32:29.461Z 📄 Base balance: « 8.63585 ETH »
2020-10-01T12:32:29.462Z 📄 Quote balance: « 0 USDT »
2020-10-01T12:32:29.463Z 📄 Ratio is: infinity
2020-10-01T12:32:29.464Z 📄 Base in quote terms is: 3172.07724275
2020-10-01T12:32:29.465Z 📄 Total in quote terms is: « 3172.07724275 USDT »
2020-10-01T12:32:29.466Z 📄 Desired base value: « 4.31792500000000000538186172 ETH »
2020-10-01T12:32:29.466Z 📄 Desired quote value: « 1586.038621375 USDT »
2020-10-01T12:32:29.467Z 📄 Change in base: « -4.31792499999999999461813828 ETH »
2020-10-01T12:32:29.468Z 📄 Sell « -4.31792499999999999461813828 ETH »
2020-10-01T12:32:29.469Z 📄 Order: {
  « Order »
    side:      sell
    orderType: limit
    clientId:  0
    owner:     CsKuESr3J5uNXaGwUhXucP3uoSPGU8pTWwb1Sa5mf2MC
    payer:     5S3YrMvnFSJoDXyEbXzrka9wrppKjBAZvAfSVTErdWiM
    price:     367.32
    size:      4.317925

2020-10-01T12:32:29.478Z 📄 Simulation - not placing order: SELL              <no ID>     367.32   4.31792
```
So the order it would be placing would sell 4.317925 ETH (half the 8.63585 ETH in our balance) at a price of 367.32 USDT (the ask price in the current spread: « 367.31 / 367.32 USDT »).

Looks good to me. Let’s do it!
```
$ /var/sammd/bin/samm rebalance --market ETH/USDT
2020-10-01T12:35:16.259Z 📄 Spread: « 367.62 / 367.65 USDT »
2020-10-01T12:35:16.263Z 📄 Mid price: 367.635 USDT
2020-10-01T12:35:16.264Z 📄 Ratio should be: 0.00272008921892638079
2020-10-01T12:35:18.066Z 📄 Base balance: « 8.63585 ETH »
2020-10-01T12:35:18.067Z 📄 Quote balance: « 0 USDT »
2020-10-01T12:35:18.068Z 📄 Ratio is: infinity
2020-10-01T12:35:18.068Z 📄 Base in quote terms is: 3174.84071475
2020-10-01T12:35:18.069Z 📄 Total in quote terms is: « 3174.84071475 USDT »
2020-10-01T12:35:18.070Z 📄 Desired base value: « 4.31792500000000000747713482625 ETH »
2020-10-01T12:35:18.071Z 📄 Desired quote value: « 1587.420357375 USDT »
2020-10-01T12:35:18.071Z 📄 Change in base: « -4.31792499999999999252286517375 ETH »
2020-10-01T12:35:18.072Z 📄 Sell « -4.31792499999999999252286517375 ETH »
2020-10-01T12:35:18.073Z 📄 Order: {
  « Order »
    side:      sell
    orderType: limit
    clientId:  0
    owner:     CsKuESr3J5uNXaGwUhXucP3uoSPGU8pTWwb1Sa5mf2MC
    payer:     5S3YrMvnFSJoDXyEbXzrka9wrppKjBAZvAfSVTErdWiM
    price:     367.65
    size:      4.317925

2020-10-01T12:35:18.082Z 📄 Placing order: SELL              <no ID>     367.65   4.31792
2020-10-01T12:35:23.416Z 📄 Order confirmed: 2UB37LX3qbBjmTSX7cFu5xNrkWYxmzrddNTmECsVwUQw9A7i8xUWBkTrtGNQobgcMbq6mgLpZq3jMz5RkDRLShbP
```
Here’s where I was going to say ‘Now we wait for the order to be filled’ but before I could even watch it on the DEX it was done.
```
/var/sammd/bin/samm balance --market ETH/USDT
2020-10-01T12:36:32.272Z 📄 Solana:                    1.97253368 SOL
2020-10-01T12:36:34.481Z 📄 Wrapped Ethereum:             4.31885 ETH
2020-10-01T12:36:34.483Z 📄 Wrapped USDT:            1583.782555 USDT (of which 1583.782555 USDT is unsettled)
2020-10-01T12:36:34.887Z 📄 Notional total:         3171.2622495 USDT
```

You can see we now have about 50% of the value in ETH, 50% of the value in USDT, and our ‘notional total’ remains about the same.

You can also see that the USDT is marked as ‘unsettled’. When an order is placed on Serum, the tokens you are exchanging are removed from your account and placed in a holding account. When the order is filled, those tokens are moved from the holding account and the tokens you received in exchange are moved to the holding account.

That’s the state we’re in now. Moving the funds from your holding account to your actual account is called ‘settling’ and we can do this now using the `settle` command:
```
$ /var/sammd/bin/samm settle --market ETH/USDT
2020-10-01T12:46:44.915Z 📄 Settling: 5abZGhrELnUnfM9ZUnvK6XJPoBU5eShZwfFPkdhAC7o 0ETH free (of 0ETH), 1583.782555USDT free (of 1583.782555USDT)
2020-10-01T12:46:45.340Z 📄 Waiting for: 3geLouSjbhEv1iBshB6X4rDiQrZfPAsfYSXmQeRHmkJQiSTop1wLkVDh65EtcDNMLcoGuQqhtYvdAd6yZdFK2QHV
2020-10-01T12:46:48.785Z 📄 Settled: 3geLouSjbhEv1iBshB6X4rDiQrZfPAsfYSXmQeRHmkJQiSTop1wLkVDh65EtcDNMLcoGuQqhtYvdAd6yZdFK2QHV
```
Now you can see that all our balances are settled:
```
$ /var/sammd/bin/samm balance --market ETH/USDT
2020-10-01T12:47:36.162Z 📄 Solana:                    1.97252868 SOL
2020-10-01T12:47:37.968Z 📄 Wrapped Ethereum:             4.31885 ETH
2020-10-01T12:47:37.970Z 📄 Wrapped USDT:            1583.782555 USDT
2020-10-01T12:47:38.455Z 📄 Notional total:         3172.1260195 USDT
```

### 9. 🎬 Start (Pretend) Making The Market!

Now you are ready to start making the market! Let’s give it a go.

In the following command, note:
* the use of `/var/sammd/bin/sammd` instead of `/var/sammd/bin/sammd`, to start the market-maker as a daemon, and
* the `--simulate` parameter - on a simulated run no actual orders will be placed.

```
$ /var/sammd/bin/sammd make-market --market ETH/USDT --simulate
af1234008869c826a4a6c3f98c34de9effb8a5ac76e004658ae5012b54e3e779
2020-10-01T13:02:02.987Z 📄 Fee is 0.30%
2020-10-01T13:02:04.741Z 📄 Base balance: « 4.31885 ETH »
2020-10-01T13:02:04.741Z 📄 Quote balance: « 1583.782555 USDT »
2020-10-01T13:02:04.741Z 📄 Position size is 0.1000%
2020-10-01T13:02:04.743Z 📄 Minimum order size of 0.001 implies minimum base liquidity of 1 Wrapped Ethereum (ETH)
2020-10-01T13:02:04.743Z 📄 Tick size is 0.01 Wrapped USDT
2020-10-01T13:02:05.243Z 📄 Current spread is « 367.25 / 367.29 USDT »
2020-10-01T13:02:05.246Z 📄 Current implied price is 365.50 Wrapped USDT for 1 Wrapped Ethereum
2020-10-01T13:02:06.236Z 📄 Balances: « « 4.31885 ETH », « 1583.782555 USDT » »
2020-10-01T13:02:06.887Z 📄 Spread: « 367.25 / 367.26 USDT »
2020-10-01T13:02:06.887Z 📄 Buy price is 365.2 - lower than best ask in spread « 367.25 / 367.26 USDT »
2020-10-01T13:02:06.887Z 📄 Sell price is 368.2 - higher than best bid in spread « 367.25 / 367.26 USDT »
2020-10-01T13:02:06.888Z 📄 Orders to place:
  « Order »
    side:      buy
    orderType: postOnly
    clientId:  1
    owner:     CsKuESr3J5uNXaGwUhXucP3uoSPGU8pTWwb1Sa5mf2MC
    payer:     EuFT3Dxhuxsav2j8BFRnkqt9HmyPvr6tvchMR53WgoYQ
    price:     365.2
    size:      0.004319

  « Order »
    side:      sell
    orderType: postOnly
    clientId:  2
    owner:     CsKuESr3J5uNXaGwUhXucP3uoSPGU8pTWwb1Sa5mf2MC
    payer:     5S3YrMvnFSJoDXyEbXzrka9wrppKjBAZvAfSVTErdWiM
    price:     368.2
    size:      0.004319

2020-10-01T13:02:06.892Z 📄 DSimulation - not placing order: BUY order                    1     365.20   0.00432
2020-10-01T13:02:06.892Z 📄 DSimulation - not placing order: SELL order                   2     368.20   0.00432
2020-10-01T13:02:06.892Z 📄 Sleeping for 30 seconds...
```

This starts SAMM running, performing the calculations to create orders, but not submitting those orders to the market. The `--simulate` parameter means the server will run and recalculate the orders frequently.

The `/var/sammd/bin/sammd` command ran `make-market` as a daemon and then ran `docker logs --follow sammd` to show the logs as they appear.

You can safely use Control-C here to stop showing you the logs - the SAMM daemon will continue to run in the background, and you can run `docker logs --follow sammd` to start watching the logs again.

To actually stop the SAMM daemon, use the command:
```
docker stop sammd
```
Important things to note from the simulated run:
* The server will check for filled orders (and cancel/create orders if necessary) every 30 seconds.
* The spread on the market was: « 367.25 / 367.29 USDT ».
* The spread we were offering was wider: « 365.2 / 368.2 USDT ».
* The fee we were taking was: 0.30%.
* The position size we were offering for both buy and sell was 0.1%, which equated to 0.004319 ETH.
* It checks to make sure neither of our prices is the wrong side of the spread. SAMM will never try to place a buy higher than the best ask, or sell lower than the best bid.

### 10. ⚡ Really Start Making The Market!

Remember that section on ☠️ Risks above? Well, if you still want to do it - **and I’m not recommending you do** - you can have `make-market` submit live orders by removing the `--simulate` parameter.

After a while, if it makes any trades, you should see a CSV of the form:

```
Timestamp,Market,Order ID,Client ID,Side,Maker,Price,Size,Fee
2020-10-01T14:19:16.556Z,ETH-USDT,673490626131135730627602,,sell,true,365.1,0.007,-0.000766
2020-10-01T14:26:12.881Z,ETH-USDT,673140137993735246891753,,buy,true,364.9,0.007,-0.000766
2020-10-01T14:34:42.087Z,ETH-USDT,673306158690398635112608,,sell,true,365,0.007,-0.000766
```

in `/var/sammd/auditlogs`. The filename is based on the market and the date, so my file is called `/var/sammd/auditlogs/ETH-USDT-2020-10-01.csv`.

In addition, SAMM writes problems to logs in the `/var/sammd/auditlogs` directory:
* Warnings are written to `/var/sammd/auditlogs/warnings.log`.
* Errors are written to `/var/sammd/auditlogs/errors.log`.
* Critical problems (such as server crashes) are written to `/var/sammd/auditlogs/critical.log`.