"use strict";

const Big = require("big.js");
const {
    expect
} = require("chai");
const fakes = require("./fakes.js");
const log = require("../lib/log.js");
const numbers = require("../lib/numbers.js");
const order = require("../lib/order.js");

describe("Order", () => {
    log.setLevels({
        trace: false,
        print: false,
        warn: false
    });

    describe("Construction", () => {
        it("Constructor should successfully construct with valid parameters", () => {
            const owner = fakes.owner();
            const payer = fakes.tokenAccount();
            const openOrdersAddress = fakes.randomAccount().publicKey;
            const actual = new order.Order(
                order.OrderSide.BUY,
                owner,
                payer,
                order.OrderType.POSTONLY,
                new Big(100),
                new Big(101),
                new Big(102),
                new Big(103),
                openOrdersAddress,
                new Big(104),
                new Big(105),
                new Big(106),
                new Big(107)
            );
            expect(actual.constructor.name).to.equal("Order");

            expect(actual.side).to.equal(order.OrderSide.BUY);
            expect(actual.owner).to.equal(owner);
            expect(actual.payer).to.equal(payer);
            expect(actual.type).to.equal(order.OrderType.POSTONLY);
            expect(actual.price.eq(new Big(100))).to.be.true;
            expect(actual.size.eq(new Big(101))).to.be.true;
            expect(actual.id.eq(new Big(102))).to.be.true;
            expect(actual.clientId.eq(new Big(103))).to.be.true;
            expect(actual.openOrdersAddress).to.equal(openOrdersAddress);
            expect(actual.openOrdersSlot.eq(new Big(104))).to.be.true;
            expect(actual.feeTier.eq(new Big(105))).to.be.true;
            expect(actual.priceLots.eq(new Big(106))).to.be.true;
            expect(actual.sizeLots.eq(new Big(107))).to.be.true;
        });

        it("Constructor should throw with invalid order side", () => {
            const shouldThrow = () => new order.Order(
                "invalid",
                fakes.owner(),
                fakes.tokenAccount(),
                order.OrderType.POSTONLY,
                new Big(100),
                new Big(101),
                new Big(102),
                new Big(103),
                fakes.randomAccount().publicKey,
                new Big(104),
                new Big(105),
                new Big(106),
                new Big(107)
            );

            expect(shouldThrow).to.throw();
        });

        it("Constructor should throw with invalid owner", () => {
            const shouldThrow = () => new order.Order(
                order.OrderSide.BUY,
                "invalid",
                fakes.tokenAccount(),
                order.OrderType.POSTONLY,
                new Big(100),
                new Big(101),
                new Big(102),
                new Big(103),
                fakes.randomAccount().publicKey,
                new Big(104),
                new Big(105),
                new Big(106),
                new Big(107)
            );

            expect(shouldThrow).to.throw();
        });

        it("Constructor should throw with invalid payer", () => {
            const shouldThrow = () => new order.Order(
                order.OrderSide.BUY,
                fakes.owner(),
                "invalid",
                order.OrderType.POSTONLY,
                new Big(100),
                new Big(101),
                new Big(102),
                new Big(103),
                fakes.randomAccount().publicKey,
                new Big(104),
                new Big(105),
                new Big(106),
                new Big(107)
            );

            expect(shouldThrow).to.throw();
        });

        it("Constructor should throw with invalid order type", () => {
            const shouldThrow = () => new order.Order(
                order.OrderSide.BUY,
                fakes.owner(),
                fakes.tokenAccount(),
                "invalid",
                new Big(100),
                new Big(101),
                new Big(102),
                new Big(103),
                fakes.randomAccount().publicKey,
                new Big(104),
                new Big(105),
                new Big(106),
                new Big(107)
            );

            expect(shouldThrow).to.throw();
        });

        it("Constructor should throw with invalid price", () => {
            const shouldThrow = () => new order.Order(
                order.OrderSide.BUY,
                fakes.owner(),
                fakes.tokenAccount(),
                order.OrderType.POSTONLY,
                "invalid",
                new Big(101),
                new Big(102),
                new Big(103),
                fakes.randomAccount().publicKey,
                new Big(104),
                new Big(105),
                new Big(106),
                new Big(107)
            );

            expect(shouldThrow).to.throw();
        });

        it("Constructor should throw with invalid size", () => {
            const shouldThrow = () => new order.Order(
                order.OrderSide.BUY,
                fakes.owner(),
                fakes.tokenAccount(),
                order.OrderType.POSTONLY,
                new Big(100),
                "invalid",
                new Big(102),
                new Big(103),
                fakes.randomAccount().publicKey,
                new Big(104),
                new Big(105),
                new Big(106),
                new Big(107)
            );

            expect(shouldThrow).to.throw();
        });

        it("Constructor should throw with invalid order Id", () => {
            const shouldThrow = () => new order.Order(
                order.OrderSide.BUY,
                fakes.owner(),
                fakes.tokenAccount(),
                order.OrderType.POSTONLY,
                new Big(100),
                new Big(101),
                "invalid",
                new Big(103),
                fakes.randomAccount().publicKey,
                new Big(104),
                new Big(105),
                new Big(106),
                new Big(107)
            );

            expect(shouldThrow).to.throw();
        });

        it("Constructor should throw with invalid client Id", () => {
            const shouldThrow = () => new order.Order(
                order.OrderSide.BUY,
                fakes.owner(),
                fakes.tokenAccount(),
                order.OrderType.POSTONLY,
                new Big(100),
                new Big(101),
                new Big(102),
                "invalid",
                fakes.randomAccount().publicKey,
                new Big(104),
                new Big(105),
                new Big(106),
                new Big(107)
            );

            expect(shouldThrow).to.throw();
        });

        it("Constructor should throw with invalid open orders address", () => {
            const shouldThrow = () => new order.Order(
                order.OrderSide.BUY,
                fakes.owner(),
                fakes.tokenAccount(),
                order.OrderType.POSTONLY,
                new Big(100),
                new Big(101),
                new Big(102),
                new Big(103),
                "invalid",
                new Big(104),
                new Big(105),
                new Big(106),
                new Big(107)
            );

            expect(shouldThrow).to.throw();
        });

        it("Constructor should throw with invalid open orders slot", () => {
            const shouldThrow = () => new order.Order(
                order.OrderSide.BUY,
                fakes.owner(),
                fakes.tokenAccount(),
                order.OrderType.POSTONLY,
                new Big(100),
                new Big(101),
                new Big(102),
                new Big(103),
                fakes.randomAccount().publicKey,
                "invalid",
                new Big(105),
                new Big(106),
                new Big(107)
            );

            expect(shouldThrow).to.throw();
        });

        it("Constructor should throw with invalid fee tier", () => {
            const shouldThrow = () => new order.Order(
                order.OrderSide.BUY,
                fakes.owner(),
                fakes.tokenAccount(),
                order.OrderType.POSTONLY,
                new Big(100),
                new Big(101),
                new Big(102),
                new Big(103),
                fakes.randomAccount().publicKey,
                new Big(104),
                "invalid",
                new Big(106),
                new Big(107)
            );

            expect(shouldThrow).to.throw();
        });

        it("Constructor should throw with invalid price lots", () => {
            const shouldThrow = () => new order.Order(
                order.OrderSide.BUY,
                fakes.owner(),
                fakes.tokenAccount(),
                order.OrderType.POSTONLY,
                new Big(100),
                new Big(101),
                new Big(102),
                new Big(103),
                fakes.randomAccount().publicKey,
                new Big(104),
                new Big(105),
                "invalid",
                new Big(107)
            );

            expect(shouldThrow).to.throw();
        });

        it("Constructor should throw with invalid size lots", () => {
            const shouldThrow = () => new order.Order(
                order.OrderSide.BUY,
                fakes.owner(),
                fakes.tokenAccount(),
                order.OrderType.POSTONLY,
                new Big(100),
                new Big(101),
                new Big(102),
                new Big(103),
                fakes.randomAccount().publicKey,
                new Big(104),
                new Big(105),
                new Big(106),
                "invalid"
            );

            expect(shouldThrow).to.throw();
        });
    });

    describe("Conversion", () => {
        it("toSerumOrder() should return converted object", () => {
            const owner = fakes.owner();
            const payer = fakes.tokenAccount();
            const openOrdersAddress = fakes.randomAccount().publicKey;
            const actual = new order.Order(
                order.OrderSide.BUY,
                owner,
                payer,
                order.OrderType.POSTONLY,
                new Big(100),
                new Big(101),
                new Big(102),
                new Big(103),
                openOrdersAddress,
                new Big(104),
                new Big(105),
                new Big(106),
                new Big(107)
            );

            const serumOrder = actual.toSerumOrder();

            expect(serumOrder.side).to.equal("buy");
            expect(serumOrder.owner).to.equal(owner);
            expect(serumOrder.payer).to.equal(payer.publicKey);
            expect(serumOrder.orderType).to.equal("postOnly");
            expect(serumOrder.price).to.equal(100);
            expect(serumOrder.size).to.equal(101);
            expect(serumOrder.clientId.eq(numbers.createBNFromBigAndDecimals(new Big(103)))).to.be.true;
        });

        it("cloneWithPrice() should return new object with different price", () => {
            const owner = fakes.owner();
            const payer = fakes.tokenAccount();
            const openOrdersAddress = fakes.randomAccount().publicKey;
            const initial = new order.Order(
                order.OrderSide.BUY,
                owner,
                payer,
                order.OrderType.POSTONLY,
                new Big(100),
                new Big(101),
                new Big(102),
                new Big(103),
                openOrdersAddress,
                new Big(104),
                new Big(105),
                new Big(106),
                new Big(107)
            );

            const updated = order.Order.cloneWithPrice(initial, new Big(200));

            expect(updated.price.eq(new Big(200))).to.be.true;
            expect(initial.price.eq(new Big(100))).to.be.true;
        });

        it("fromSerum() from basic order should return new object with correct properties", () => {
            const actual = order.Order.fromSerum({
                side: "buy",
                price: 1000,
                size: 50,
                feeTier: 0
            });

            expect(actual.side).to.equal(order.OrderSide.BUY);
            expect(actual.price.eq(new Big(1000))).to.be.true;
            expect(actual.size.eq(new Big(50))).to.be.true;
            expect(actual.feeTier.eq(new Big(0))).to.be.true;
        });

        it("fromSerum() from full order should return new object with correct properties", () => {
            const serumNumber = (num) => numbers.createBNFromBigAndDecimals(new Big(num));
            const owner = fakes.owner();
            const payer = fakes.randomPublicKey();
            const openOrdersAddress = fakes.randomAccount().publicKey;
            const actual = order.Order.fromSerum({
                side: "buy",
                owner,
                payer,
                orderType: "limit",
                price: 1000,
                size: 50,
                orderId: serumNumber(27),
                clientId: serumNumber(28),
                openOrdersAddress,
                openOrdersSlot: serumNumber(29),
                feeTier: serumNumber(30),
                priceLots: serumNumber(31),
                sizeLots: serumNumber(32)
            });

            expect(actual.side).to.equal(order.OrderSide.BUY);
            expect(actual.owner).to.equal(owner);
            expect(actual.payer.publicKey).to.equal(payer);
            expect(actual.type).to.equal(order.OrderType.LIMIT);
            expect(actual.price.eq(new Big(1000))).to.be.true;
            expect(actual.size.eq(new Big(50))).to.be.true;
            expect(actual.id.eq(new Big(27))).to.be.true;
            expect(actual.clientId.eq(new Big(28))).to.be.true;
            expect(actual.openOrdersAddress).to.equal(openOrdersAddress);
            expect(actual.openOrdersSlot.eq(new Big(29))).to.be.true;
            expect(actual.feeTier.eq(new Big(30))).to.be.true;
            expect(actual.priceLots.eq(new Big(31))).to.be.true;
            expect(actual.sizeLots.eq(new Big(32))).to.be.true;
        });
    });
});