"use strict";

const {
    expect
} = require("chai");
const fakes = require("./fakes.js");
const log = require("../lib/log.js");
const settler = require("../lib/settler.js");
const sinon = require("sinon");
const numbers = require("../lib/numbers.js");

describe("Settler", () => {
    log.setLevels({
        trace: false,
        print: false
    });

    describe("SimulationSettler", () => {
        it("Constructor should succeed with no parameters", () => {
            const settle = new settler.SimulationSettler();
            expect(settle.constructor.name).to.equal("SimulationSettler");
        });

        it("settle() should not throw", async () => {
            const settle = new settler.SimulationSettler();
            await settle.settle();
        });
    });

    describe("AsyncSettler", () => {
        it("Constructor should succeed with valid parameters", () => {
            const context = fakes.context();
            const owner = fakes.owner();
            const market = fakes.market();
            const baseAccount = fakes.tokenAccount();
            const quoteAccount = fakes.tokenAccount();
            const settle = new settler.AsyncSettler(context, owner, market, baseAccount, quoteAccount);
            expect(settle.constructor.name).to.equal("AsyncSettler");
        });

        it("Constructor should fail with invalid context", () => {
            const owner = fakes.owner();
            const market = fakes.market();
            const baseAccount = fakes.tokenAccount();
            const quoteAccount = fakes.tokenAccount();
            const shouldThrow = () => new settler.AsyncSettler("invalid", owner, market, baseAccount, quoteAccount);
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid owner", () => {
            const context = fakes.context();
            const market = fakes.market();
            const baseAccount = fakes.tokenAccount();
            const quoteAccount = fakes.tokenAccount();
            // eslint-disable-next-line max-len
            const shouldThrow = () => new settler.AsyncSettler(context, "invalid", market, baseAccount, quoteAccount);
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid market", () => {
            const context = fakes.context();
            const owner = fakes.owner();
            const baseAccount = fakes.tokenAccount();
            const quoteAccount = fakes.tokenAccount();
            const shouldThrow = () => new settler.AsyncSettler(context, owner, "invalid", baseAccount, quoteAccount);
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid base token account", () => {
            const context = fakes.context();
            const owner = fakes.owner();
            const market = fakes.market();
            const quoteAccount = fakes.tokenAccount();
            const shouldThrow = () => new settler.AsyncSettler(context, owner, market, "invalid", quoteAccount);
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid quote token account", () => {
            const context = fakes.context();
            const owner = fakes.owner();
            const market = fakes.market();
            const baseAccount = fakes.tokenAccount();
            const shouldThrow = () => new settler.AsyncSettler(context, owner, market, baseAccount, "invalid");
            expect(shouldThrow).to.throw();
        });

        it("settle() should not call settleFunds() if no openorders accounts", async () => {
            const context = fakes.context();
            const owner = fakes.owner();
            const market = fakes.market();
            const baseAccount = fakes.tokenAccount();
            const quoteAccount = fakes.tokenAccount();
            const settle = new settler.AsyncSettler(context, owner, market, baseAccount, quoteAccount);
            await settle.settle();
            expect(market.typedFindOpenOrdersAccountsForOwner.called).to.be.true;
            expect(market.typedSettleFunds.called).to.be.false;
            expect(context.connection.waitFor.called).to.be.false;
        });

        it("settle() should not call settleFunds() if OpenOrders exist but have zero value", async () => {
            const context = fakes.context();
            const owner = fakes.owner();
            const market = fakes.market();
            const baseAccount = fakes.tokenAccount();
            const quoteAccount = fakes.tokenAccount();
            const settle = new settler.AsyncSettler(context, owner, market, baseAccount, quoteAccount);
            const fakeOpenOrders1 = fakes.openOrders({
                baseTokenTotal: numbers.BNZero,
                baseTokenFree: numbers.BNZero,
                quoteTokenTotal: numbers.BNZero,
                quoteTokenFree: numbers.BNZero
            });
            const fakeOpenOrders2 = fakes.openOrders({
                baseTokenTotal: numbers.BNZero,
                baseTokenFree: numbers.BNZero,
                quoteTokenTotal: numbers.BNZero,
                quoteTokenFree: numbers.BNZero
            });
            market.typedFindOpenOrdersAccountsForOwner = sinon.fake.returns(Promise.resolve([
                fakeOpenOrders1,
                fakeOpenOrders2
            ]));
            await settle.settle();
            expect(market.typedFindOpenOrdersAccountsForOwner.called).to.be.true;
            expect(market.typedSettleFunds.called).to.be.false;
            expect(context.connection.waitFor.called).to.be.false;
        });

        it("settle() should call settleFunds() but not wait", async () => {
            const context = fakes.context();
            const owner = fakes.owner();
            const market = fakes.market();
            const baseAccount = fakes.tokenAccount();
            const quoteAccount = fakes.tokenAccount();
            const settle = new settler.AsyncSettler(context, owner, market, baseAccount, quoteAccount);
            const fakeOpenOrders1 = fakes.openOrders({
                baseTokenTotal: numbers.BNOne,
                baseTokenFree: numbers.BNOne,
                quoteTokenTotal: numbers.BNOne,
                quoteTokenFree: numbers.BNOne
            });
            const fakeOpenOrders2 = fakes.openOrders({
                baseTokenTotal: numbers.BNOne,
                baseTokenFree: numbers.BNOne,
                quoteTokenTotal: numbers.BNOne,
                quoteTokenFree: numbers.BNOne
            });
            market.typedFindOpenOrdersAccountsForOwner = sinon.fake.returns(Promise.resolve([
                fakeOpenOrders1,
                fakeOpenOrders2
            ]));
            await settle.settle();
            expect(market.typedFindOpenOrdersAccountsForOwner.called).to.be.true;
            expect(market.typedSettleFunds.calledTwice).to.be.true;
            expect(context.connection.waitFor.called).to.be.false;
        });
    });

    describe("SyncSettler", () => {
        it("Constructor should succeed with valid parameters", () => {
            const context = fakes.context();
            const owner = fakes.owner();
            const market = fakes.market();
            const baseAccount = fakes.tokenAccount();
            const quoteAccount = fakes.tokenAccount();
            const settle = new settler.SyncSettler(context, owner, market, baseAccount, quoteAccount);
            expect(settle.constructor.name).to.equal("SyncSettler");
        });

        it("Constructor should fail with invalid context", () => {
            const owner = fakes.owner();
            const market = fakes.market();
            const baseAccount = fakes.tokenAccount();
            const quoteAccount = fakes.tokenAccount();
            const shouldThrow = () => new settler.SyncSettler("invalid", owner, market, baseAccount, quoteAccount);
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid owner", () => {
            const context = fakes.context();
            const market = fakes.market();
            const baseAccount = fakes.tokenAccount();
            const quoteAccount = fakes.tokenAccount();
            // eslint-disable-next-line max-len
            const shouldThrow = () => new settler.SyncSettler(context, "invalid", market, baseAccount, quoteAccount);
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid market", () => {
            const context = fakes.context();
            const owner = fakes.owner();
            const baseAccount = fakes.tokenAccount();
            const quoteAccount = fakes.tokenAccount();
            const shouldThrow = () => new settler.SyncSettler(context, owner, "invalid", baseAccount, quoteAccount);
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid base token account", () => {
            const context = fakes.context();
            const owner = fakes.owner();
            const market = fakes.market();
            const quoteAccount = fakes.tokenAccount();
            const shouldThrow = () => new settler.SyncSettler(context, owner, market, "invalid", quoteAccount);
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid quote token account", () => {
            const context = fakes.context();
            const owner = fakes.owner();
            const market = fakes.market();
            const baseAccount = fakes.tokenAccount();
            const shouldThrow = () => new settler.SyncSettler(context, owner, market, baseAccount, "invalid");
            expect(shouldThrow).to.throw();
        });

        it("settle() should not call settleFunds() if no openorders accounts", async () => {
            const context = fakes.context();
            const owner = fakes.owner();
            const market = fakes.market();
            const baseAccount = fakes.tokenAccount();
            const quoteAccount = fakes.tokenAccount();
            const settle = new settler.SyncSettler(context, owner, market, baseAccount, quoteAccount);
            await settle.settle();
            expect(market.typedFindOpenOrdersAccountsForOwner.called).to.be.true;
            expect(market.typedSettleFunds.called).to.be.false;
            expect(context.connection.waitFor.called).to.be.false;
        });

        it("settle() should not call settleFunds() if OpenOrders exist but have zero value", async () => {
            const context = fakes.context();
            const owner = fakes.owner();
            const market = fakes.market();
            const baseAccount = fakes.tokenAccount();
            const quoteAccount = fakes.tokenAccount();
            const settle = new settler.SyncSettler(context, owner, market, baseAccount, quoteAccount);
            const fakeOpenOrders1 = fakes.openOrders({
                baseTokenTotal: numbers.BNZero,
                baseTokenFree: numbers.BNZero,
                quoteTokenTotal: numbers.BNZero,
                quoteTokenFree: numbers.BNZero
            });
            const fakeOpenOrders2 = fakes.openOrders({
                baseTokenTotal: numbers.BNZero,
                baseTokenFree: numbers.BNZero,
                quoteTokenTotal: numbers.BNZero,
                quoteTokenFree: numbers.BNZero
            });
            market.typedFindOpenOrdersAccountsForOwner = sinon.fake.returns(Promise.resolve([
                fakeOpenOrders1,
                fakeOpenOrders2
            ]));
            await settle.settle();
            expect(market.typedFindOpenOrdersAccountsForOwner.called).to.be.true;
            expect(market.typedSettleFunds.called).to.be.false;
            expect(context.connection.waitFor.called).to.be.false;
        });

        it("settle() should call settleFunds() and wait", async () => {
            const context = fakes.context();
            const owner = fakes.owner();
            const market = fakes.market();
            const baseAccount = fakes.tokenAccount();
            const quoteAccount = fakes.tokenAccount();
            const settle = new settler.SyncSettler(context, owner, market, baseAccount, quoteAccount);
            const fakeOpenOrders1 = fakes.openOrders({
                baseTokenTotal: numbers.BNOne,
                baseTokenFree: numbers.BNOne,
                quoteTokenTotal: numbers.BNOne,
                quoteTokenFree: numbers.BNOne
            });
            const fakeOpenOrders2 = fakes.openOrders({
                baseTokenTotal: numbers.BNOne,
                baseTokenFree: numbers.BNOne,
                quoteTokenTotal: numbers.BNOne,
                quoteTokenFree: numbers.BNOne
            });
            market.typedFindOpenOrdersAccountsForOwner = sinon.fake.returns(Promise.resolve([
                fakeOpenOrders1,
                fakeOpenOrders2
            ]));
            await settle.settle();
            expect(market.typedFindOpenOrdersAccountsForOwner.called).to.be.true;
            expect(market.typedSettleFunds.calledTwice).to.be.true;
            expect(context.connection.waitFor.called).to.be.true;
        });
    });

    describe("AutoRefreshSettler", () => {
        it("Constructor should succeed with valid parameters", () => {
            const every = 10;
            const factory = () => new settler.SimulationSettler();
            const settle = new settler.AutoRefreshSettler(every, factory);
            expect(settle.constructor.name).to.equal("AutoRefreshSettler");
        });

        it("Constructor should fail with invalid refresh interval", () => {
            const factory = () => new settler.SimulationSettler();
            const shouldThrow = () => new settler.AutoRefreshSettler("invalid", factory);
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with zero refresh interval", () => {
            const every = 0;
            const factory = () => new settler.SimulationSettler();
            const shouldThrow = () => new settler.AutoRefreshSettler(every, factory);
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with negative refresh interval", () => {
            const every = -10;
            const factory = () => new settler.SimulationSettler();
            const shouldThrow = () => new settler.AutoRefreshSettler(every, factory);
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid factory", () => {
            const every = 10;
            const shouldThrow = () => new settler.AutoRefreshSettler(every, "invalid");
            expect(shouldThrow).to.throw();
        });

        it("settle() should refresh every time if refresh interval is 1", async () => {
            const every = 1;
            const factory = sinon.spy(() => new settler.SimulationSettler());
            const settle = new settler.AutoRefreshSettler(every, factory);
            expect(settle.constructor.name).to.equal("AutoRefreshSettler");
            for (let index = 0; index < 10; index += 1) {
                expect(factory.callCount).to.equal(index);
                // eslint-disable-next-line no-await-in-loop
                await settle.settle();
                expect(factory.callCount).to.equal(index + 1);
            }

            expect(factory.callCount).to.equal(10);
        });

        it("settle() should refresh every 10 times if refresh interval is 10", async () => {
            const every = 10;
            const factory = sinon.spy(() => new settler.SimulationSettler());
            const settle = new settler.AutoRefreshSettler(every, factory);
            expect(settle.constructor.name).to.equal("AutoRefreshSettler");
            for (let index = 0; index < 100; index += 1) {
                // eslint-disable-next-line no-await-in-loop
                await settle.settle();
            }

            expect(factory.callCount).to.equal(10);
        });
    });
});