"use strict";

const Big = require("big.js");
const {
    expect
} = require("chai");
const fakes = require("./fakes.js");
const log = require("../lib/log.js");
const order = require("../lib/order.js");
const orderbuilder = require("../lib/orderbuilder.js");
const ordertracker = require("../lib/ordertracker.js");

describe("OrderBuilder", () => {
    log.setLevels({
        trace: false,
        print: false,
        warn: false
    });

    describe("ConstantProductOrderBuilder", () => {
        it("Constructor should successfully construct with valid parameters", async () => {
            const accountTracker = await fakes.accountTracker();
            const orderTracker = new ordertracker.OrderTracker();
            const orderBuilder = new orderbuilder.ConstantProductOrderBuilder(
                fakes.market(),
                accountTracker,
                orderTracker,
                fakes.owner(),
                fakes.positionSizeProportion(),
                fakes.fee()
            );
            expect(orderBuilder.constructor.name).to.equal("ConstantProductOrderBuilder");
        });

        it("Constructor should throw with invalid market", async () => {
            const accountTracker = await fakes.accountTracker();
            const orderTracker = new ordertracker.OrderTracker();
            const shouldThrow = () => new orderbuilder.ConstantProductOrderBuilder(
                "x",
                accountTracker,
                orderTracker,
                fakes.owner(),
                fakes.positionSizeProportion(),
                fakes.fee()
            );
            expect(shouldThrow).to.throw();
        });

        it("Constructor should throw with invalid account tracker", () => {
            const orderTracker = new ordertracker.OrderTracker();
            const shouldThrow = () => new orderbuilder.ConstantProductOrderBuilder(
                fakes.market(),
                "x",
                orderTracker,
                fakes.owner(),
                fakes.positionSizeProportion(),
                fakes.fee()
            );
            expect(shouldThrow).to.throw();
        });

        it("Constructor should throw with invalid order tracker", async () => {
            const accountTracker = await fakes.accountTracker();
            const shouldThrow = () => new orderbuilder.ConstantProductOrderBuilder(
                fakes.market(),
                accountTracker,
                "x",
                fakes.owner(),
                fakes.positionSizeProportion(),
                fakes.fee()
            );
            expect(shouldThrow).to.throw();
        });

        it("Constructor should throw with invalid owner", async () => {
            const accountTracker = await fakes.accountTracker();
            const orderTracker = new ordertracker.OrderTracker();
            const shouldThrow = () => new orderbuilder.ConstantProductOrderBuilder(
                fakes.market(),
                accountTracker,
                orderTracker,
                "x",
                fakes.positionSizeProportion(),
                fakes.fee()
            );
            expect(shouldThrow).to.throw();
        });

        it("Constructor should throw with invalid position size proportion", async () => {
            const accountTracker = await fakes.accountTracker();
            const orderTracker = new ordertracker.OrderTracker();
            const shouldThrow = () => new orderbuilder.ConstantProductOrderBuilder(
                fakes.market(),
                accountTracker,
                orderTracker,
                fakes.owner(),
                "x",
                fakes.fee()
            );
            expect(shouldThrow).to.throw();
        });

        it("Constructor should throw with invalid fakes.fee()", async () => {
            const accountTracker = await fakes.accountTracker();
            const orderTracker = new ordertracker.OrderTracker();
            const shouldThrow = () => new orderbuilder.ConstantProductOrderBuilder(
                fakes.market(),
                accountTracker,
                orderTracker,
                fakes.owner(),
                fakes.positionSizeProportion(),
                "x"
            );
            expect(shouldThrow).to.throw();
        });

        it("buildOrders() should return valid buy and sell orders", async () => {
            const accountTracker = await fakes.accountTracker();
            const orderTracker = new ordertracker.OrderTracker();
            const owner = fakes.owner();
            const orderBuilder = new orderbuilder.ConstantProductOrderBuilder(
                fakes.market(),
                accountTracker,
                orderTracker,
                owner,
                fakes.positionSizeProportion(),
                fakes.fee()
            );
            const balance = fakes.balance(new Big(100), new Big(100));
            const actual = await orderBuilder.buildOrders(balance);
            const expectedBuy = new order.Order(
                order.OrderSide.BUY,
                owner,
                accountTracker.quoteTokenAccounts[0],
                order.OrderType.POSTONLY,
                new Big(0.998003),
                new Big(0.1),
                null,
                new Big(1)
            );
            const expectedSell = new order.Order(
                order.OrderSide.SELL,
                owner,
                accountTracker.baseTokenAccounts[0],
                order.OrderType.POSTONLY,
                new Big(1.002003),
                new Big(0.1),
                null,
                new Big(2)
            );
            expect(fakes.ordersMatch(expectedBuy, actual[0])).to.be.true;
            expect(fakes.ordersMatch(expectedSell, actual[1])).to.be.true;
        });
    });

    describe("SpreadAdjustingOrderBuilder", () => {
        it("Constructor should successfully construct with valid parameters", () => {
            const orderBuilder = new orderbuilder.SpreadAdjustingOrderBuilder(
                fakes.orderBuilder(),
                fakes.spreadFetcher(),
                fakes.tickSize()
            );
            expect(orderBuilder.constructor.name).to.equal("SpreadAdjustingOrderBuilder");
        });

        it("Constructor should throw with invalid order builder", () => {
            const shouldThrow = () => new orderbuilder.SpreadAdjustingOrderBuilder(
                "x",
                fakes.spreadFetcher(),
                fakes.tickSize()
            );
            expect(shouldThrow).to.throw();
        });

        it("Constructor should throw with invalid spread fetcher", () => {
            const shouldThrow = () => new orderbuilder.SpreadAdjustingOrderBuilder(
                fakes.orderBuilder(),
                "x",
                fakes.tickSize()
            );
            expect(shouldThrow).to.throw();
        });

        it("Constructor should throw with invalid tick size", () => {
            const shouldThrow = () => new orderbuilder.SpreadAdjustingOrderBuilder(
                fakes.orderBuilder(),
                fakes.spreadFetcher(),
                "x"
            );
            expect(shouldThrow).to.throw();
        });

        it("buildOrders() should not adjust order when outside of spread", async () => {
            const owner = fakes.owner();
            const payer = fakes.tokenAccount();
            const providedBuys = [
                new order.Order(
                    order.OrderSide.BUY,
                    owner,
                    payer,
                    order.OrderType.POSTONLY,
                    new Big(98),
                    new Big(0.1),
                    null,
                    new Big(11)
                )
            ];
            const providedSells = [
                new order.Order(
                    order.OrderSide.SELL,
                    owner,
                    payer,
                    order.OrderType.POSTONLY,
                    new Big(102),
                    new Big(0.1),
                    null,
                    new Big(12)
                )
            ];

            const fakeSpread = fakes.spread(new Big(99), new Big(101));
            const fakeSpreadFetcher = fakes.spreadFetcher(fakeSpread);
            const fakeOrderBuilder = fakes.orderBuilder(providedBuys, providedSells);

            const orderBuilder = new orderbuilder.SpreadAdjustingOrderBuilder(
                fakeOrderBuilder,
                fakeSpreadFetcher,
                fakes.tickSize()
            );
            const balance = fakes.balance(new Big(100), new Big(100));

            const actual = await orderBuilder.buildOrders(balance);

            expect(fakes.ordersMatch(providedBuys[0], actual[0])).to.be.true;
            expect(fakes.ordersMatch(providedSells[0], actual[1])).to.be.true;
        });

        it("buildOrders() should not adjust order when within spread", async () => {
            const owner = fakes.owner();
            const payer = fakes.tokenAccount();
            const providedBuys = [
                new order.Order(
                    order.OrderSide.BUY,
                    owner,
                    payer,
                    order.OrderType.POSTONLY,
                    new Big(99),
                    new Big(0.1),
                    null,
                    new Big(11)
                )
            ];
            const providedSells = [
                new order.Order(
                    order.OrderSide.SELL,
                    owner,
                    payer,
                    order.OrderType.POSTONLY,
                    new Big(101),
                    new Big(0.1),
                    null,
                    new Big(12)
                )
            ];

            const fakeOrderBuilder = fakes.orderBuilder(providedBuys, providedSells);
            const fakeSpread = fakes.spread(new Big(98), new Big(102));
            const fakesSpreadFetcher = fakes.spreadFetcher(fakeSpread);

            const orderBuilder = new orderbuilder.SpreadAdjustingOrderBuilder(
                fakeOrderBuilder,
                fakesSpreadFetcher,
                fakes.tickSize()
            );
            const balance = fakes.balance(new Big(100), new Big(100));

            const actual = await orderBuilder.buildOrders(balance);

            expect(fakes.ordersMatch(providedBuys[0], actual[0])).to.be.true;
            expect(fakes.ordersMatch(providedSells[0], actual[1])).to.be.true;
        });

        it("buildOrders() should adjust buy order when ask is above the best ask", async () => {
            const owner = fakes.owner();
            const payer = fakes.tokenAccount();
            const providedBuys = [
                new order.Order(
                    order.OrderSide.BUY,
                    owner,
                    payer,
                    order.OrderType.POSTONLY,
                    new Big(103),
                    new Big(0.1),
                    null,
                    new Big(11)
                )
            ];
            const providedSells = [
                new order.Order(
                    order.OrderSide.SELL,
                    owner,
                    payer,
                    order.OrderType.POSTONLY,
                    new Big(104),
                    new Big(0.1),
                    null,
                    new Big(12)
                )
            ];
            const fakeOrderBuilder = fakes.orderBuilder(providedBuys, providedSells);
            const fakeSpread = fakes.spread(new Big(99), new Big(101));
            const fakeSpreadFetcher = fakes.spreadFetcher(fakeSpread);

            const orderBuilder = new orderbuilder.SpreadAdjustingOrderBuilder(
                fakeOrderBuilder,
                fakeSpreadFetcher,
                fakes.tickSize()
            );
            const balance = fakes.balance(new Big(100), new Big(100));

            const actual = await orderBuilder.buildOrders(balance);

            // The provided buy price was 103. The spread was 99-101. If we bought for 103, we'd
            // be losing money. Instead we adjust the price to be within the spread. We go 1 tick
            // above the spread's current best bid price. (Other options would be 1 tick lower
            // than the current best ask price, or just the mid price.)
            const expectedBuy = new order.Order(
                order.OrderSide.BUY,
                owner,
                payer,
                order.OrderType.POSTONLY,
                // 1 tick above best bid
                new Big(99.0001),
                new Big(0.1),
                null,
                new Big(11)
            );
            expect(fakes.ordersMatch(expectedBuy, actual[0])).to.be.true;
            expect(fakes.ordersMatch(providedSells[0], actual[1])).to.be.true;
        });

        it("buildOrders() should adjust sell order when bid is below the best bid", async () => {
            const owner = fakes.owner();
            const payer = fakes.tokenAccount();
            const providedBuys = [
                new order.Order(
                    order.OrderSide.BUY,
                    owner,
                    payer,
                    order.OrderType.POSTONLY,
                    new Big(97),
                    new Big(0.1),
                    null,
                    new Big(11)
                )
            ];
            const providedSells = [
                new order.Order(
                    order.OrderSide.SELL,
                    owner,
                    payer,
                    order.OrderType.POSTONLY,
                    new Big(98),
                    new Big(0.1),
                    null,
                    new Big(12)
                )
            ];
            const fakeOrderBuilder = fakes.orderBuilder(providedBuys, providedSells);
            const fakeSpread = fakes.spread(new Big(99), new Big(101));
            const fakeSpreadFetcher = fakes.spreadFetcher(fakeSpread);

            const orderBuilder = new orderbuilder.SpreadAdjustingOrderBuilder(
                fakeOrderBuilder,
                fakeSpreadFetcher,
                fakes.tickSize()
            );
            const balance = fakes.balance(new Big(100), new Big(100));

            const actual = await orderBuilder.buildOrders(balance);

            // The provided sell price was 98. The spread was 99-101. If we sold for 98, we'd be
            // losing money. Instead we adjust the price to be within the spread. We go 1 tick
            // below the spread's current best asking price. (Other options would be 1 tick higher
            // than the current best bid price, or just the mid price.)
            const expectedSell = new order.Order(
                order.OrderSide.SELL,
                owner,
                payer,
                order.OrderType.POSTONLY,
                // 1 tick below best ask
                new Big(100.9999),
                new Big(0.1),
                null,
                new Big(12)
            );
            expect(fakes.ordersMatch(providedBuys[0], actual[0])).to.be.true;
            expect(fakes.ordersMatch(expectedSell, actual[1])).to.be.true;
        });
    });

    describe("PositionSizeFilteringOrderBuilder", () => {
        it("Constructor should successfully construct with valid parameters", () => {
            const orderBuilder = new orderbuilder.PositionSizeFilteringOrderBuilder(fakes.orderBuilder());
            expect(orderBuilder.constructor.name).to.equal("PositionSizeFilteringOrderBuilder");
        });

        it("Constructor should throw with invalid order builder", () => {
            const shouldThrow = () => new orderbuilder.PositionSizeFilteringOrderBuilder("x");
            expect(shouldThrow).to.throw();
        });

        it("buildOrders() should not adjust order when size OK", async () => {
            const owner = fakes.owner();
            const payer = fakes.tokenAccount();
            const providedBuys = [
                new order.Order(
                    order.OrderSide.BUY,
                    owner,
                    payer,
                    order.OrderType.POSTONLY,
                    new Big(99),
                    new Big(0.1),
                    null,
                    new Big(11)
                )
            ];
            const providedSells = [
                new order.Order(
                    order.OrderSide.SELL,
                    owner,
                    payer,
                    order.OrderType.POSTONLY,
                    new Big(101),
                    new Big(0.1),
                    null,
                    new Big(12)
                )
            ];

            const fakeOrderBuilder = fakes.orderBuilder(providedBuys, providedSells);
            const orderBuilder = new orderbuilder.PositionSizeFilteringOrderBuilder(fakeOrderBuilder);
            const balance = fakes.balance(new Big(100), new Big(100));

            const actual = await orderBuilder.buildOrders(balance);

            expect(fakes.ordersMatch(providedBuys[0], actual[0])).to.be.true;
            expect(fakes.ordersMatch(providedSells[0], actual[1])).to.be.true;
        });

        it("buildOrders() should remove buy when its size is too big", async () => {
            const owner = fakes.owner();
            const payer = fakes.tokenAccount();
            const providedBuys = [
                new order.Order(
                    order.OrderSide.BUY,
                    owner,
                    payer,
                    order.OrderType.POSTONLY,
                    new Big(99),
                    new Big(20),
                    null,
                    new Big(11)
                )
            ];
            const providedSells = [
                new order.Order(
                    order.OrderSide.SELL,
                    owner,
                    payer,
                    order.OrderType.POSTONLY,
                    new Big(101),
                    new Big(0.1),
                    null,
                    new Big(12)
                )
            ];

            const fakeOrderBuilder = fakes.orderBuilder(providedBuys, providedSells);
            const orderBuilder = new orderbuilder.PositionSizeFilteringOrderBuilder(fakeOrderBuilder);
            const balances = fakes.balance(new Big(10), new Big(10));

            const actual = await orderBuilder.buildOrders(balances);

            expect(actual.length).to.equal(1);
            expect(fakes.ordersMatch(providedSells[0], actual[0])).to.be.true;
        });

        it("buildOrders() should remove sell when its too big", async () => {
            const owner = fakes.owner();
            const payer = fakes.tokenAccount();
            const providedBuys = [
                new order.Order(
                    order.OrderSide.BUY,
                    owner,
                    payer,
                    order.OrderType.POSTONLY,
                    new Big(99),
                    new Big(0.1),
                    null,
                    new Big(11)
                )
            ];
            const providedSells = [
                new order.Order(
                    order.OrderSide.SELL,
                    owner,
                    payer,
                    order.OrderType.POSTONLY,
                    new Big(101),
                    new Big(20),
                    null,
                    new Big(12)
                )
            ];
            const fakeOrderBuilder = fakes.orderBuilder(providedBuys, providedSells);
            const orderBuilder = new orderbuilder.PositionSizeFilteringOrderBuilder(fakeOrderBuilder);
            const balances = fakes.balance(new Big(10), new Big(10));

            const actual = await orderBuilder.buildOrders(balances);

            expect(actual.length).to.equal(1);
            expect(fakes.ordersMatch(providedBuys[0], actual[0])).to.be.true;
        });

        it("buildOrders() should remove both buy and sell when they are both too big", async () => {
            const owner = fakes.owner();
            const payer = fakes.tokenAccount();
            const providedBuys = [
                new order.Order(
                    order.OrderSide.BUY,
                    owner,
                    payer,
                    order.OrderType.POSTONLY,
                    new Big(99),
                    new Big(20),
                    null,
                    new Big(11)
                )
            ];
            const providedSells = [
                new order.Order(
                    order.OrderSide.SELL,
                    owner,
                    payer,
                    order.OrderType.POSTONLY,
                    new Big(101),
                    new Big(20),
                    null,
                    new Big(12)
                )
            ];
            const fakeOrderBuilder = fakes.orderBuilder(providedBuys, providedSells);
            const orderBuilder = new orderbuilder.PositionSizeFilteringOrderBuilder(fakeOrderBuilder);
            const balances = fakes.balance(new Big(10), new Big(10));

            const actual = await orderBuilder.buildOrders(balances);

            expect(actual.length).to.equal(0);
        });
    });
});