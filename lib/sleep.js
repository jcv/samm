"use strict";

const log = require("../lib/log.js");
const numbers = require("../lib/numbers.js");

function sleepForMilliseconds(ms) {
    log.trace(`Sleeping for ${ms / numbers.MILLISECONDS_TO_SECONDS_FACTOR} seconds...`);
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}

function sleepForSeconds(seconds) {
    return sleepForMilliseconds(seconds * numbers.MILLISECONDS_TO_SECONDS_FACTOR);
}

module.exports = {
    sleepForSeconds,
    sleepForMilliseconds
};