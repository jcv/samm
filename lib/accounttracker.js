"use strict";

const assert = require("./assert");
const log = require("./log");
const serum = require("@project-serum/serum");
const tags = require("./tags");
const token = require("./token");

class AccountTracker {
    constructor(baseTokenAccounts, quoteTokenAccounts) {
        assert.notConstructingAbstractType(AccountTracker, new.target);

        // eslint-disable-next-line max-len
        assert.parameterTypeIsArrayOfType("AccountTracker()", "baseTokenAccounts", baseTokenAccounts, token.TokenAccount);
        // eslint-disable-next-line max-len
        assert.parameterTypeIsArrayOfType("AccountTracker()", "quoteTokenAccounts", quoteTokenAccounts, token.TokenAccount);

        this.isAccountTracker = true;

        this.baseTokenAccounts = baseTokenAccounts;
        this.quoteTokenAccounts = quoteTokenAccounts;
    }

    get [Symbol.toStringTag]() {
        return this.constructor?.name;
    }

    isOneOfMyOpenOrdersAccounts() {
        assert.throwOnAbstractCall(AccountTracker, this.isOneOfMyOpenOrdersAccounts);
    }

    tag() {
        assert.throwOnAbstractCall(AccountTracker, this.tag);
    }
}

class SimulationAccountTracker extends AccountTracker {
    constructor(baseTokenAccounts, quoteTokenAccounts, valueToReturn) {
        super(baseTokenAccounts, quoteTokenAccounts);

        assert.parameterTypeOf("SimulationAccountTracker()", "valueToReturn", valueToReturn, "boolean");

        this.valueToReturn = Boolean(valueToReturn);

        if (new.target === SimulationAccountTracker) {
            Object.freeze(this);
        }
    }

    isOneOfMyOpenOrdersAccounts() {
        return this.valueToReturn;
    }

    // eslint-disable-next-line class-methods-use-this
    tag(item) {
        return item;
    }
}

class SyncAccountTracker extends AccountTracker {
    constructor(baseTokenAccounts, quoteTokenAccounts, openOrdersAccounts) {
        super(baseTokenAccounts, quoteTokenAccounts);

        // eslint-disable-next-line max-len
        assert.parameterTypeIsArrayOfType("SyncAccountTracker()", "openOrdersAccounts", openOrdersAccounts, serum.OpenOrders);

        this.openOrdersAccounts = openOrdersAccounts;

        if (new.target === SyncAccountTracker) {
            Object.freeze(this);
        }
    }

    isOneOfMyOpenOrdersAccounts(accountToCheck) {
        for (const mine of this.openOrdersAccounts) {
            if (accountToCheck.equals(mine.address)) {
                return true;
            }
        }

        return false;
    }

    tag(item) {
        if (item) {
            if (item.openOrders) {
                if (this.isOneOfMyOpenOrdersAccounts(item.openOrders)) {
                    tags.CommonTagOperations.mine(item, "openorders");
                } else {
                    tags.CommonTagOperations.notMine(item, "openorders");
                }
            }
        }

        return item;
    }
}

function traceAddress(name, address) {
    log.trace(`Tracking ${name} account: ${address}`);
}

async function createAccountTracker(context, market, owner) {
    const baseTokenAccounts = await market.typedFindBaseTokenAccountsForOwner(owner.publicKey);
    for (const baseTokenAccount of baseTokenAccounts) {
        traceAddress("base token", baseTokenAccount);
    }

    const quoteTokenAccounts = await market.typedFindQuoteTokenAccountsForOwner(owner.publicKey);
    for (const quoteTokenAccount of quoteTokenAccounts) {
        traceAddress("quote token", quoteTokenAccount);
    }

    if (context.simulate) {
        return new SimulationAccountTracker(baseTokenAccounts, quoteTokenAccounts, false);
    }

    const openOrdersAccounts = await market.typedFindOpenOrdersAccountsForOwner(owner.publicKey);
    for (const openOrdersAccount of openOrdersAccounts.map((ooa) => ooa.address)) {
        traceAddress("open orders", openOrdersAccount);
    }

    return new SyncAccountTracker(baseTokenAccounts, quoteTokenAccounts, openOrdersAccounts);
}

function buildOpenOrdersTagger(openOrdersTracker) {
    return new tags.FunctionTagger((item) => {
        if (item) {
            if (item.openOrders) {
                if (openOrdersTracker.isOneOfMyOpenOrdersAccounts(item.openOrders)) {
                    tags.CommonTagOperations.mine(item, "openorders");
                } else {
                    tags.CommonTagOperations.notMine(item, "openorders");
                }
            }
        }

        return item;
    });
}

async function createAccountTagger(context, market, owner) {
    const tracker = await createAccountTracker(context, market, owner);
    return buildOpenOrdersTagger(tracker);
}

// Common helper function for our events, fills and requests commands.
async function createOnlyMineAccountTracker(context, market) {
    if (context.onlyMine) {
        const owner = context.loadAccount();

        const baseTokenAccounts = await market.typedFindBaseTokenAccountsForOwner(owner.publicKey);
        const quoteTokenAccounts = await market.typedFindQuoteTokenAccountsForOwner(owner.publicKey);
        const openOrdersAccounts = await market.typedFindOpenOrdersAccountsForOwner(owner.publicKey);

        return new SyncAccountTracker(baseTokenAccounts, quoteTokenAccounts, openOrdersAccounts);
    }

    return new SimulationAccountTracker([], [], true);
}

async function createOnlyMineFilter(context, market) {
    const tracker = await createOnlyMineAccountTracker(context, market);
    return (item) => tracker.isOneOfMyOpenOrdersAccounts(item.openOrders);
}

module.exports = {
    AccountTracker,
    SimulationAccountTracker,
    SyncAccountTracker,
    createAccountTracker,
    buildOpenOrdersTagger,
    createAccountTagger,
    createOnlyMineAccountTracker,
    createOnlyMineFilter
};