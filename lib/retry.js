"use strict";

const assert = require("./assert.js");
const log = require("./log.js");
const sleep = require("./sleep.js");

class Retrier {
    constructor(retries) {
        assert.parameterTypeIsArray("Retrier()", "retries", retries);

        this.retries = retries;
    }

    async withRetries(name, func) {
        const lastTrySentinal = {};
        const retryDelays = [...this.retries, lastTrySentinal];
        let counter = 0;
        for (const delay of retryDelays) {
            try {
                counter += 1;

                // Yes, we really do want an await in a loop. Parallelising here
                // would be bad.
                // eslint-disable-next-line no-await-in-loop
                return await func();
            } catch (ex) {
                if (delay === lastTrySentinal) {
                    throw ex;
                }

                log.warn(`withRetries() failure calling '${name}', retry count:`, counter, ex);
                // Yes, we really do want an await in a loop. Parallelising here
                // would be bad.
                // eslint-disable-next-line no-await-in-loop
                await sleep.sleepForSeconds(delay);
                log.warn(`withRetries() retrying '${name}' after a delay of ${delay} seconds.`);
            }
        }

        throw new Error(`withRetries() failure calling '${name}' - unexpected end of retry loop.`);
    }
}

module.exports = {
    Retrier
};