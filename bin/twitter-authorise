#! /usr/bin/env node

"use strict";

const ctx = require("../lib/context.js");
const inquirer = require("inquirer");
const log = require("../lib/log.js");
const process = require("process");
const twit = require("../lib/twitter.js");

async function main() {
    const context = await ctx.loadContext(process.argv);
    const twitter = twit.buildTwitterOauthClient(context);

    const response = await twitter.getRequestToken("oob");
    log.trace(`OAuth Token: ${response.oauth_token}`);
    log.trace(`OAuth Token Secret: ${response.oauth_token_secret}`);

    log.print(`Go to https://api.twitter.com/oauth/authenticate?oauth_token=${response.oauth_token}`);

    const question = {
        type: "input",
        name: "oauthPin",
        message: "Now enter the OAuth PIN (shown after authorizing on the above web page):"
    };

    const answers = await inquirer.prompt([question]);
    const {oauthPin} = answers;
    const accessTokenResponse = await twitter.getAccessToken({
        // eslint-disable-next-line camelcase
        oauth_verifier: oauthPin,
        // eslint-disable-next-line camelcase
        oauth_token: response.oauth_token
    });
    log.print(`Access User: ${accessTokenResponse.screen_name}`);
    log.print(`TWITTER_ACCESS_TOKEN_KEY: ${accessTokenResponse.oauth_token}`);
    log.print(`TWITTER_ACCESS_TOKEN_SECRET: ${accessTokenResponse.oauth_token_secret}`);
}

main().catch((ex) => log.critical(ex));